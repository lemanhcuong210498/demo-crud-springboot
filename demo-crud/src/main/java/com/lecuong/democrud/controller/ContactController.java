package com.lecuong.democrud.controller;

import com.lecuong.democrud.entity.Contact;
import com.lecuong.democrud.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping({"/contact"})
public class ContactController {

    @Autowired
    private ContactService contactService;

    @GetMapping
    public String listContact(ModelMap modelMap){
        modelMap.addAttribute("listContact", contactService.findAll());
        return "list";
    }

    @GetMapping("/search-contact")
    public String searchContact(@RequestParam String term, ModelMap modelMap){
        if (StringUtils.isEmpty(term)){
            return "redirect:/contact";
        }
        modelMap.addAttribute("listContact", contactService.search(term));
        return "list";
    }

    @GetMapping("/add-contact")
    public String addContact(ModelMap modelMap){
        modelMap.addAttribute("contact", new Contact());
        return "form";
    }

    @PostMapping("/save-contact")
    public String saveContact(@Valid Contact contact, BindingResult result, RedirectAttributes redirect){
        if (result.hasErrors()){
            return "form";
        }

        contactService.save(contact);
        redirect.addFlashAttribute("successMessage", "Saved contact successfully!");
        return "redirect:/contact";
    }

    @GetMapping("/edit-contact/{id}")
    public String editContact(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("contact", contactService.findOne(id));
        return "form";
    }

    @GetMapping("/delete-contact/{id}")
    public String delete(@PathVariable Long id, RedirectAttributes redirect) {
        contactService.delete(id);
        redirect.addFlashAttribute("successMessage", "Deleted contact successfully!");
        return "redirect:/contact";
    }
}
