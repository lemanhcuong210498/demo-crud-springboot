package com.lecuong.democrud.controller;

import com.lecuong.democrud.entity.User;
import com.lecuong.democrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping
    public String loginPage() {
        return "login";
    }

    @PostMapping
    public String checkLogin(@RequestParam String email,
                             @RequestParam String password,
                             ModelMap modelMap) {

        User user = userService.findUserByEmail(email);
        if (user == null) {
            return "login";
        }
        if (!passwordEncoder.matches(password, user.getPassword())) {
            return "login";
        }

        return "redirect:/home";
    }

    @GetMapping("/403")
    public String errorPage(){
        return "403";
    }
}
