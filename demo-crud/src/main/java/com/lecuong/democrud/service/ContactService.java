package com.lecuong.democrud.service;

import com.lecuong.democrud.entity.Contact;

import java.util.List;
import java.util.Optional;

public interface ContactService {

    Iterable<Contact> findAll();

    List<Contact> search(String term);

    Optional<Contact> findOne(Long id);

    void save(Contact contact);

    void delete(Long id);
}
